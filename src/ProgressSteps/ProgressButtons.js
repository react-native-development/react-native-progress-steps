import React from 'react';
import { View } from 'react-native';
const ProgressButtons = props => (
  <View style={{ flexDirection: 'row', marginTop: 50 }}>
    <View style={{ position: 'absolute', left: 20, bottom: 10, width: '27%' }}>{props.renderPreviousButton()}</View>
    <View style={{ position: 'absolute', right: props.right, bottom: 10, width: props.rightWidth }}>{props.renderNextButton()}</View>
  </View>
);

export default ProgressButtons;