import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity, Text, KeyboardAvoidingView, Platform } from 'react-native';
import PropTypes from 'prop-types';
import { Button } from 'native-base';
import ProgressButtons from './ProgressButtons';
import theme from '../../../../App/assets/css/appTheme';
import Entypo from 'react-native-vector-icons/Entypo';
import { Colors } from '../../../../App/assets/Themes/';
import FontsFamily from '../../../../App/assets/Themes/FontsFamily';
class ProgressStep extends Component {
  onNextStep = async () => {
    this.props.onNext && (await this.props.onNext());

    // Return out of method before moving to next step if errors exist.
    if (this.props.errors) {
      return;
    }

    this.props.setActiveStep(this.props.activeStep + 1);
    if (this.props.scrollable) {
      setTimeout(() => {
        this.scrollListReftop.scrollTo({ x: 0, y: 0, animated: true })
      }, 1000);

    }
  };

  onPreviousStep = () => {
    // Changes active index and calls previous function passed by parent
    this.props.onPrevious && this.props.onPrevious();
    this.props.setActiveStep(this.props.activeStep - 1);
  };

  onSubmit = () => {
    this.props.onSubmit && this.props.onSubmit();
  };

  renderNextButton = () => {
    const btnStyle = {
      textAlign: 'center',
      padding: 8,
      ...this.props.nextBtnStyle
    };

    const btnTextStyle = {
      color: '#007AFF',
      fontSize: 18,
      ...this.props.nextBtnTextStyle
    };

    const disabledBtnText = {
      color: '#cdcdcd'
    };

    let textStyle = [btnTextStyle];
    if (this.props.nextBtnDisabled) textStyle.push(disabledBtnText);

    return (
      <TouchableOpacity
        style={{
          // width: this.props.activeStep === this.props.stepCount - 1 ? '120%' : '100%',
          width: this.props.activeStep === this.props.stepCount - 1 ? '120%' : '120%',

          ...theme.appBackgroundColor, justifyContent: 'center',
          height: 30,
          borderRadius: 20,
          flexDirection: 'row',
          alignItems: 'center',
          // backgroundColor: this.props.activeStep === this.props.stepCount - 1 ? this.props.nextBtnDisabled ? Colors.gray : Colors.parrotColor : Colors.white,
          backgroundColor: this.props.activeStep === this.props.stepCount - 1 ? this.props.nextBtnDisabled ? Colors.gray : Colors.parrotColor : Colors.parrotColor,

        }}
        onPress={this.props.activeStep === this.props.stepCount - 1 ? this.onSubmit : this.onNextStep}
        disabled={this.props.nextBtnDisabled}

      >

        <Text style={{
          color: 'black',
          // fontWeight: 'bold'
          ...FontsFamily.style.Gotham_Bold
        }}>

          {this.props.activeStep === this.props.stepCount - 1 ? this.props.nextBtnDisabled ? '' : this.props.finishBtnText.toUpperCase() : this.props.nextBtnText.toUpperCase()}


        </Text>
        {/* <Entypo name={"chevron-small-right"} size={27} color={"white"} /> */}
      </TouchableOpacity>
      // <TouchableOpacity
      // style={btnStyle}
      // onPress={this.props.activeStep === this.props.stepCount - 1 ? this.onSubmit : this.onNextStep}
      // disabled={this.props.nextBtnDisabled}
      // >
      //   <Text style={textStyle}>
      //     {this.props.activeStep === this.props.stepCount - 1 ? this.props.finishBtnText : this.props.nextBtnText}
      //   </Text>
      // </TouchableOpacity>
    );
  };

  renderPreviousButton = () => {
    const btnStyle = {
      textAlign: 'center',
      padding: 8,
      ...this.props.previousBtnStyle
    };

    const btnTextStyle = {
      color: '#007AFF',
      fontSize: 18,
      ...this.props.previousBtnTextStyle
    };

    const disabledBtnText = {
      color: '#cdcdcd'
    };

    let textStyle = [btnTextStyle];
    if (this.props.previousBtnDisabled) textStyle.push(disabledBtnText);

    return (
      <TouchableOpacity
        style={{
          // textAlign: 'center',
          ...theme.appBackgroundColor, justifyContent: 'center',
          height: 30,
          borderRadius: 20,
          // width: '100%',
          alignItems: 'center',
          flexDirection: 'row',
          backgroundColor: 'black',
        }}
        onPress={this.onPreviousStep}
        disabled={this.props.previousBtnDisabled}
      //  disabled={this.props.nextBtnDisabled}
      >

        {/* <Entypo name={"chevron-small-left"} size={27} color={"white"} /> */}
        <Text style={{
          color: 'white',
          // fontWeight: 'bold'
          ...FontsFamily.style.Gotham_Bold
        }}>

          {this.props.activeStep === 0 ? 'Back'.toUpperCase() : this.props.previousBtnText.toUpperCase()}
        </Text>
      </TouchableOpacity>
      // <TouchableOpacity style={btnStyle} onPress={this.onPreviousStep} disabled={this.props.previousBtnDisabled}>
      //   <Text style={textStyle}>{this.props.activeStep === 0 ? '' : this.props.previousBtnText}</Text>
      // </TouchableOpacity>
    );
  };

  render() {
    const scrollViewProps = this.props.scrollViewProps || {};
    const viewProps = this.props.viewProps || {};
    const isScrollable = this.props.scrollable;
    const buttonRow = this.props.removeBtnRow ? null : (
      <ProgressButtons
        renderNextButton={this.renderNextButton}
        // right={this.props.activeStep === this.props.stepCount - 1 ? 55 : 20}
        // rightWidth={this.props.activeStep === this.props.stepCount - 1 ? '47%' : '27%'}

        right={55}
        rightWidth={'47%'}
        renderPreviousButton={this.renderPreviousButton}
      />
    );

    return (
      <View style={{ flex: 1 }}>
        {isScrollable
          ?
          // <View />
          Platform.OS === 'ios' ?
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled keyboardVerticalOffset={200}>

              <ScrollView {...scrollViewProps} ref={(ref) => { this.scrollListReftop = ref; }}>{this.props.children}</ScrollView>
            </KeyboardAvoidingView>
            :
            <ScrollView {...scrollViewProps} ref={(ref) => { this.scrollListReftop = ref; }}>{this.props.children}</ScrollView>

          :


          <View {...viewProps}>{this.props.children}</View>


        }
        <View style={{ backgroundColor: Colors.gray, justifyContent: 'center' }}>
          {buttonRow}
        </View>
      </View>
    );
  }
}

ProgressStep.propTypes = {
  label: PropTypes.string,
  onNext: PropTypes.func,
  onPrevious: PropTypes.func,
  onSubmit: PropTypes.func,
  setActiveStep: PropTypes.func,
  nextBtnText: PropTypes.string,
  previousBtnText: PropTypes.string,
  finishBtnText: PropTypes.string,
  stepCount: PropTypes.number,
  nextBtnStyle: PropTypes.object,
  nextBtnTextStyle: PropTypes.object,
  nextBtnDisabled: PropTypes.bool,
  previousBtnStyle: PropTypes.object,
  previousBtnTextStyle: PropTypes.object,
  previousBtnDisabled: PropTypes.bool,
  scrollViewProps: PropTypes.object,
  viewProps: PropTypes.object,
  errors: PropTypes.bool,
  removeBtnRow: PropTypes.bool,
  scrollable: PropTypes.bool
};

ProgressStep.defaultProps = {
  nextBtnText: 'Next',
  previousBtnText: 'Back',
  finishBtnText: 'Submit',
  nextBtnDisabled: false,
  previousBtnDisabled: false,
  errors: false,
  removeBtnRow: false,
  scrollable: true
};

export default ProgressStep;